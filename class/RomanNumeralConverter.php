<?php
/*
 *  RomanNumeralConverter Class
 */
class RomanNumeralConverter {

    const ROMAN_NUMERALS_SINGLE = array(
        'M' => 1000,
        'D' => 500,
        'C' => 100,
        'L' => 50,
        'X' => 10,
        'V' => 5,
        'I' => 1,
    );

    const ROMAN_NUMERALS_SIMPLE = array(
        'M' => 1000,
        'D' => 500,
        'C' => 100,
        'L' => 50,
        'X' => 10,
        'V' => 5,
    );

    const ROMAN_NUMERALS_COMPLEX = array(
        'CM' => 900,
        'CD' => 400,
        'XC' => 90,
        'XL' => 40,
        'IX' => 9,
        'IV' => 4,
    );

    /**
     * The Single converter is expected to deal with integers resulting in a single Roman digit. Low numbers up to 1000.
     *
     * @param int $integer
     * @return int|boolean
     */
    function lowIntegerConverter($integer)
    {
        //  ignore values less than 1 and greater than 1000
        if ( $integer < 1 || $integer > 1000 ) {
            false;
        }

        $result = array_search ( $integer , self::ROMAN_NUMERALS_SINGLE );
        // The Roman numeral should be built, return it
        return $result;
    }

    /**
     * Simple integer conversion is expected to deal with integers without 4’s or 9’s. Low numbers up to 1000
     *
     * @param int $integer
     * @return int|boolean
     */
    function simpleIntegerConverter($integer)
    {
        if ($result = $this->lowIntegerConverter($integer)) {
            return $result;
        }

        // Passed value contains 4 and 9 return false
        if (strpos($integer, '4') || strpos($integer, '9') ) { // return false
            return false;
        }

        // Merge mapping array for single and simple
        $romanDataArray = array_merge(self::ROMAN_NUMERALS_SINGLE,self::ROMAN_NUMERALS_SIMPLE);

        $result = '';
        foreach($romanDataArray as $roman => $value){
            // Determine the number of matches
            $matches = intval($integer/$value);

            // Add the same number of characters to the string
            $result .= str_repeat($roman,$matches);

            // Set the integer to be the remainder of the integer and the value
            $integer = $integer % $value;
        }
        return $result;
    }

    /**
     * Complex integers involves 4 or 9. Low numbers up to 1000. Example list is supplied.
     * Complex romans have a lower digit before the higher digit. E.g. IV means 5-1.
     *Low numbers up to 1000.
     *
     * @param int $integer
     * @return int|boolean
     */
    function complexIntegerConverter($integer)
    {
        $result = false;
        if ($result = $this->simpleIntegerConverter($integer) ) {
            return $result;
        }

        // Merge mapping array for single, simple and complex
        $romanDataArray = array_merge(self::ROMAN_NUMERALS_SINGLE,self::ROMAN_NUMERALS_SIMPLE, self::ROMAN_NUMERALS_COMPLEX);

        foreach($romanDataArray as $roman => $value){
            // Determine the number of matches
            $matches = intval($integer/$value);

            // Add the same number of characters to the string
            $result .= str_repeat($roman, $matches);

            // Set the integer to be the remainder of the integer and the value
            $integer = $integer % $value;
        }
        return $result;
    }
}