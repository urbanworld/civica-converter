<?php

require_once 'class/RomanNumeralConverter.php';

    //Instantiate convertor
    $romanNumerialConverter = new RomanNumeralConverter();

    // Low Integer Converter
    echo 'Low Integer Converter: ';
    echo $romanNumerialConverter->lowIntegerConverter(1);
    echo ' <br> ';

    // Simple Integer Converter
    echo 'Simple Integer Converter: ';
    echo $romanNumerialConverter->simpleIntegerConverter(33);
    echo ' <br> ';

    // Complex Integer Converter
    echo 'Complex Integer Converter: ';
    echo $romanNumerialConverter->complexIntegerConverter(944);
    echo ' <br> ';




