# Roman Numeral Converter


index.php - provides demo output of each one of the required functions

form.php  -  The Input form (Input a value and click submit for the response) - sends AJAX Post request to form-response.php

form-response.php - Handles the post request and returns JSON formatted response

class/RomanNumeralConverter.php - Class which handles value conversion

# SCREEN SHOTS
In the screenshots directory

## User Stores and Overview Below

User Stories
The goal of the exercise is to make a system that converts Integers into Roman Numerals. There are a number of different aspects to this challenge and the client requirements for this are defined in the stories below. This is not expected to be completed within the time limit set.

In general, the client is looking to be able to expose this functionality to other systems, ensure that the roman numerals generated conform to known standards and a novice user has a means to access the information via a simple interface. 

##RN-0001 – Low Integer to Single Roman Converter

The Single converter is expected to deal with integers resulting in a single Roman digit. Low numbers up to 1000. Comprehensive list is supplied.

Given I send the integer <input_integer>
When I receive the response
Then I get the <output_roman>

Input_integer
Output_roman
1
I
5
V
10
X
50
L
100
C
500
D
1000
M


##RN-0002 – Low Integer to Simple Roman Converter

The simple integer conversion is expected to deal with integers without 4’s or 9’s. Low numbers up to 1000. Example list is supplied.
Dependent on completion of RN-0001.

Given I send the integer <input_integer>
When I receive the response
Then I get the <output_roman>

Input_integer
Output_roman
3
III
8
VIII	
16
XVI
78
LXXVIII
255
CCLV
512
DXII
777
DCCLXXVII

##RN-0003 – Low Integer to Complex Roman Converter

Complex integers involves 4 or 9. Low numbers up to 1000. Example list is supplied.
Complex romans have a lower digit before the higher digit. E.g. IV means 5-1.
Example list is supplied. 
Dependent on completion of RN-0002.

Given I send the integer <input_integer>
When I receive the response
Then I get the <output_roman>

Input_integer
Output_roman
4
IV
9
IX	
49
XLIX
129
CXXIX
245
CCXLV
488
CDLXXXVIII
999
CMXCIX


##RN-0004 – Simple Interface

This is a simple interface for a novice user to be able to submit a request, without needing to directly interrogate the system. This deals with just the view page.
No wireframe supplied, very basic interface expected with no decoration.

Given I have entered the URL for the roman numerals converter system
When I submit the request
Then I see an input field for the integer value
  And I see an output section for the numeral value
  And I see a label for each section
  And I see a simple text instruction for system use
  And I see a submission button

##RN-0005 – Simple Interface Submission

This is a simple interface for a novice user to be able to submit a request, without needing to directly interrogate the system.  This deals with the submission request and displaying the response on the screen.
For the population, use the tables from tickets RN-0001, RN-0002 and RN-0003 when each is completed. If none are completed, a stub endpoint can be used to return a hard coded value.

Given I am viewing the roman numerals converter system
  And I have filled in the input field with an <input_integer>
When I submit the request
Then I see the <output_roman> in the output section

