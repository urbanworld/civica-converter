<?php
/* form-response.php
 *  Accepts post request with integer value
 *  returns JSON response object
 */

require_once 'class/RomanNumeralConverter.php';

   if ($_POST) {
       if (isset($_POST['integer'])) {
           //Instantiate Roman convertor
           $romanNumeralConverter = new RomanNumeralConverter();

            $integer = $_POST['integer'];
            $response = $romanNumeralConverter->complexIntegerConverter($integer);

            //Set JSON respone header
            header('Content-Type: application/json');
            if ($response) {
                $data = ['status'=> true, 'message'=> $response];
            } else{
                $data = ['status'=> true, 'message'=> 'Invalid Input'];
            }

            // output response data
            echo  json_encode($data);
       }
   } else { // Invalid Request
       // Set 400 response code
       http_response_code(400);
       header('Content-Type: application/json');
       header('Status: 400 Bad Request');

       // return the encoded json
       echo  json_encode(array(
           'status' => 400, // success or not?
           'message' => 'Bad Request'
       ));
   }





