<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Roman Interface</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

</head>
<body>
    <p>
        Input an integer value in the form field below and click submit to see the Roman Numeral value
    </p>

    <form id="romanForm" name="romanForm" method="post" action="form-response.php">
        <ul>
            <li>
                <label>Integer Value <span class="required">*</span></label>
                <input type="text" name="integer" />
            </li>
            <li>
                <input type="submit" value="Submit" />
            </li>
        </ul>
    </form>

    <div id="valueBlock">
        <h3>Roman Numeral Value:</h3>
        <div id="response">

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#valueBlock").hide();
            // Attach a submit handler to the form
            $("#romanForm").submit(function (event) {

                // Stop form from submitting normally
                event.preventDefault();

                $.post("form-response.php", $("#romanForm").serialize())
                    .done(function (data) {
                        $("#valueBlock").show();
                        console.log(data.message);
                        $("#response").text(data.message);
                    });
            });
        });
    </script>
</body>

</html>



